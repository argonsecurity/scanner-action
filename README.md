# Bitbucket Pipelines Pipe: Argon Action

Protect the first phase of your software supply chain

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: argonsecurity/scanner-action:v1.0
  variables:
    ARGON_TOKEN: "<string>"
    # SCANNERS: "<string>" # Optional.
	# AUDIT_ONLY: "<boolean>" # Optional.
    # SHOULD_NOTIFY: "<boolean>" # Optional.
```

## Variables

| Variable              | Usage                                     |
| --------------        | ----------------------------------------- |
| ARGON_TOKEN (\*)      | Token provided by argon. This is sensitive, add this as a Secured variable. |
| SCANNERS              | Comma-separated list of checks to run. Defaults to all. |
| AUDIT_ONLY            | If true the pipe will never fail the pipeline. Default: `false`. |
| SHOULD_NOTIFY         | If true a notification will be sent to the configured Slack/Teams channel on new findings. Default: `false`. |

_(\*) = required variable._

## Details

By integrating Argon into Bitbucket Pipelines you can continuously protect the first phase of your software supply chain.

To find out more about Argon, head over to [our website](https://argon.io).


## Prerequisites

Argon Token is necessary to use this pipe.

- If you don't yet have an account on argon.io, head over and [contact us](https://argon.io/contact-us).
- To obtain a token log in to your Argon account and obtain a token from the [appsec page](https://app.argon.io/appsec).
- Add the token as a [secured environment variable](https://confluence.atlassian.com/x/0CVbLw#Environmentvariables-Securedvariables) in Bitbucket Pipelines.

## Examples

### Basic scan example
Uses Argon to scan an application and break the build if any vulnerabilities found.

```yaml
script:
  - npm install

  - npm test

  - pipe: argonsecurity/scanner-action:v1.0
    variables:
      ARGON_TOKEN: $ARGON_TOKEN

  - npm publish
```

### Advanced example
Uses Argon to scan the pull request but not failing the build if found any. Also sends the vulnerability to the connected slack/teams.

```yaml
script:
  - npm install

  - npm test

  - pipe: argonsecurity/scanner-action:v1.0
    variables:
      ARGON_TOKEN: $ARGON_TOKEN
      AUDIT_ONLY: "true"
      SHOULD_NOTIFY: "true"

  - docker build -t $IMAGE_NAME .

  - docker push $IMAGE_NAME

```

## Support
If you’d like help with this pipe, or you have an issue or feature request, please contact us at: [support@argon.io](support@argon.io).

If you’re reporting an issue, please include:

* the version of the pipe
* relevant logs and error messages
* steps to reproduce
